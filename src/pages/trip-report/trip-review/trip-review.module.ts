import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TripReviewPage } from './trip-review';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    TripReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(TripReviewPage),
    SelectSearchableModule,
    TranslateModule.forChild()
  ],
})
export class TripReviewPageModule {}
